package com.company;

import clases.CapacidadEndedudamiento;
import clases.Mensajes;

import java.util.Scanner;

public class Main {
     static Scanner in = new Scanner(System.in);
    //Recuerda que aca empieza todo
    public static void main(String[] args) {
        //Con este objeto de la clase Scanner puedes capturar informacion por consola cada ves que lo uses
        // recuerda cerrar el flujo de consulta cada ves lo uses sobre para que los uses in.close()


        CapacidadEndedudamiento capacidadEndedudamiento = new CapacidadEndedudamiento();

        //Ingreso valores consola
        capacidadEndedudamiento.setIngresosTotales(Integer.valueOf(solicitudValor(Mensajes.INGRESOS_TOTALES)));
        capacidadEndedudamiento.setgastosFijos(Integer.valueOf(solicitudValor(Mensajes.GASTOS_FIJOS)));
        capacidadEndedudamiento.setgastosVariables(Integer.valueOf(solicitudValor(Mensajes.GASTOS_VARIABLES)));

        //Imprimir la capacidad endeudamiento
        System.out.println(Mensajes.CAPACIDAD_ENDEUDAMIENTO + capacidadEndedudamiento.getCapacidadEndeudamiento());

    }

    public static String solicitudValor (String tipo){
        String solicitudValor;
        do {
            System.out.println(Mensajes.VALOR + tipo + Mensajes.FINALIZAR);
            solicitudValor = in.nextLine();
            if (solicitudValor.equals("finalizar")){
                System.exit(0);
            }

        } while (!isNumeric(solicitudValor));
        return solicitudValor;
    }

    public static boolean isNumeric(String value) {
        // implementa un bloque try catch aca
        try {
            Integer.valueOf(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }

    }
}
