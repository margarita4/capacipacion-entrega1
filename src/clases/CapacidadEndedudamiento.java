package clases;

public class CapacidadEndedudamiento {
    Integer ingresosTotales;
    Integer gastosFijos;
    Integer gastoVariables;
    final double POR_FIJO = 0.35;

    //Metodos getter and setter para la clase
    public void setIngresosTotales (Integer ingresosTotales){
        this.ingresosTotales = ingresosTotales;
    }

    public void setgastosFijos(Integer gastosFijos){
         this.gastosFijos = gastosFijos;
    }

    public void setgastosVariables (Integer gastoVariables){
        this.gastoVariables = gastoVariables;
    }

    //getter
    public Integer getIngresosTotales(){
        return this.ingresosTotales;
    }

    public Integer getGastosFijos(){
         return this.gastosFijos;
    }

    public Integer getgastoVariable(){
        return this.gastoVariables;
    }

    //Construye un metodo que retorne una cadena con las propiedades de la clase

    public String getCapacidadEndeudamiento() {
        // retornar la capacidad de endeudamiento puede ser una cadena con el valor
        double capacidadEndeudamiento = (ingresosTotales - gastosFijos - gastoVariables)* POR_FIJO;
       return String.valueOf(capacidadEndeudamiento);
    }
}
